package com.dimox.springboot.webapp.controllers;

import org.apache.tomcat.util.http.fileupload.FileUploadBase.SizeLimitExceededException;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@ControllerAdvice
public class GlobalExceptionHandler {
	
	@ExceptionHandler(MultipartException.class)
	public String handleError1 (MultipartException e, RedirectAttributes flash) {		
		flash.addAttribute("error", e.getCause().getMessage());
		return "redirect:/categorias/formGuardar";
	}
	
	@ExceptionHandler(IllegalStateException.class)
	public String handleError2 (IllegalStateException e, RedirectAttributes redirectAttributes) {		
		redirectAttributes.addAttribute("error", e.getCause().getMessage());
		return "redirect:/categorias/formGuardar";
	}
	
	@ExceptionHandler(FileUploadException.class)
	public String handleError3 (FileUploadException e, RedirectAttributes redirectAttributes) {		
		redirectAttributes.addAttribute("error", e.getCause().getMessage());
		return "redirect:/categorias/formGuardar";
	}
	
	@ExceptionHandler(SizeLimitExceededException.class)
	public String handleError4 (SizeLimitExceededException e, RedirectAttributes redirectAttributes) {		
		redirectAttributes.addAttribute("error", e.getCause().getMessage());
		return "redirect:/categorias/formGuardar";
	}
	

}
