package com.dimox.springboot.webapp.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {
	
	@Value("${texto.indexcontroller.tituloIndex}")
	private String tituloIndex;

	@GetMapping ({ "/index", "/", "inicio" })
	public String index(Model model) {
		model.addAttribute("titulo", tituloIndex);
		model.addAttribute("classActiveLinkIndex","nav-link active");
		return "index";
	}
	
	
}
