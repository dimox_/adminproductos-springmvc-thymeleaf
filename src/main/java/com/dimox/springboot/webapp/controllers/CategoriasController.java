package com.dimox.springboot.webapp.controllers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.dimox.springboot.webapp.models.entities.Categoria;
import com.dimox.springboot.webapp.models.services.ICategoriaService;
import com.dimox.springboot.webapp.models.services.IProductoService;
import com.dimox.springboot.webapp.util.paginator.PageRender;

@Controller
public class CategoriasController {

	private static final String UPLOADED_FOLDER = "uploads/categorias";
	private static final int NUM_REG_PAG = 10;
	private static final int MAX_SIZE_FILE_UPLOAD = 2*1024*1024; // 2 Mb
	
	private final Logger log = LoggerFactory.getLogger(getClass());
			
			
	@Autowired
	private ICategoriaService categoriaService;

	@Value("${texto.indexcontroller.tituloCategorias}")
	private String tituloCategorias;

	@Value("${texto.indexcontroller.tituloGuardarCategoria}")
	private String tituloGuardarCategoria;

	@GetMapping("/categorias")
	public String categorias(@RequestParam(name = "page", defaultValue = "0") Integer page, Model model) {

		Pageable pageRequest = PageRequest.of(page, NUM_REG_PAG);

		Page<Categoria> categorias = categoriaService.findPaginated(pageRequest);

		PageRender<Categoria> pageRender = new PageRender<>("/categorias", categorias);

		model.addAttribute("categorias", categorias);
		model.addAttribute("classActiveLinkCategorias","nav-link active");
		model.addAttribute("titulo", tituloCategorias);
		model.addAttribute("page", pageRender);
		return "listado-categorias";
	}

	@GetMapping("/categorias/formGuardar")
	public String formularioGuardar(Model model) {
		model.addAttribute("botonSubmit", "Guardar");
		model.addAttribute("titulo", tituloGuardarCategoria);
		model.addAttribute("tituloFormulario", "Crear nueva categoría");
		model.addAttribute("categoria", new Categoria());
		return "form-guardar-categorias";
	}

	@GetMapping("/categorias/formEditar/{id}")
	public String formularioEditar(@PathVariable Long id, Model model, RedirectAttributes flash) {
		Categoria categoria = null;
		if (id > 0) {
			categoria = categoriaService.findOne(id);
			if (categoria == null) {
				flash.addFlashAttribute("error", "La categoría no existe. ");
				return "redirect:/categorias";
			}
		} else {
			flash.addFlashAttribute("error", "El ID de la categoría no es válido. ");
			return "redirect:/categorias";
		}
		model.addAttribute("botonSubmit", "Editar");
		model.addAttribute("titulo", tituloGuardarCategoria);
		model.addAttribute("tituloFormulario", "Editar categoría");
		model.addAttribute("categoria", categoria);
		return "form-guardar-categorias";
	}

	@PostMapping("/categorias/save")
	public String guardarActualizar(@Valid Categoria categoria, BindingResult bindingResult, Model model,
			@RequestParam("file") MultipartFile imagen, RedirectAttributes flash) {

		if (bindingResult.hasErrors()) {
			model.addAttribute("botonSubmit", "Guardar");
			model.addAttribute("tituloFormulario", "Crear nueva categoría");
			model.addAttribute("titulo", tituloGuardarCategoria);
			return "form-guardar-categorias";
		}

		// Obtenemos el tipo de archivo
		String TypeFile = identifyFileTypeUsingFilesProbeContentType(imagen.getOriginalFilename());
		log.info(TypeFile);

		if (!imagen.isEmpty()) {

			if (imagen.getSize() > MAX_SIZE_FILE_UPLOAD) {
				flash.addFlashAttribute("error", "El tamaño de la imagen no puede ser superior a 2Mb");
				return "redirect:/categorias/formGuardar";

			} else if (!TypeFile.equalsIgnoreCase("image/jpeg") && !TypeFile.equalsIgnoreCase("image/png")
					&& !TypeFile.equalsIgnoreCase("application/pdf")) {

				flash.addFlashAttribute("error", "Solo se admiten archivos con los siguientes formatos: jpg, png, pdf");
				return "redirect:/categorias/formGuardar";

			} else {

				
				String uniqueFilename = UUID.randomUUID().toString() + "_" + imagen.getOriginalFilename();
				Path rootPath = Paths.get(UPLOADED_FOLDER).resolve(uniqueFilename);
				Path absolutePath = rootPath.toAbsolutePath();
				
				log.info("rootPath: " + rootPath);
				log.info("absolutePath: " + absolutePath);

				try {
					
					Files.copy(imagen.getInputStream(), absolutePath);
					flash.addFlashAttribute("success",
							"La imagen '" + uniqueFilename + "' se ha subido satisfactóriamente !");

					categoria.setImagen_cat(uniqueFilename);

				} catch (IOException e) {
					System.out.println("IOexception: " + e.getMessage());
					flash.addFlashAttribute("error", "Ha ocurrido un error al subir la imagen");
					model.addAttribute("botonSubmit", "Editar");
					return "redirect:/categorias/formGuardar";
				}
			}

		} else {
			flash.addFlashAttribute("error", "Seleccione una imagen para la categoría");
			model.addAttribute("botonSubmit", "Editar");
			model.addAttribute("titulo", tituloGuardarCategoria);
			model.addAttribute("tituloFormulario", "Editar categoría");
			model.addAttribute("categoria", categoria);
			return "redirect:/categorias/formEditar/" + categoria.getId_cat();
		}

		// Si llega el id será una modificación, en caso contrario se creará nueva
		String mensageFlash = (categoria.getId_cat() != null) ? "La categoría ha sido modificada con éxito!"
				: "La categoría ha sido creada con éxito!";
		try {
			categoriaService.save(categoria);
			flash.addFlashAttribute("success", mensageFlash);
		} catch (Exception e) {
			flash.addFlashAttribute("error", "La categoría ya existe, introduzca otro nombre");
		}

		return "redirect:/categorias";
	}

	@GetMapping("/categorias/delete/{id}")
	public String delete(@PathVariable Long id, RedirectAttributes flash) {

		try {
			
			categoriaService.delete(id);
			flash.addFlashAttribute("success", "La categoría ha sido eliminada con éxito! ");
		} catch (Exception e) {
			flash.addFlashAttribute("error", "No se ha podido eliminar la categoría seleccionada");
		}
		return "redirect:/categorias";
	}
	
	// PENDING TO REFACTOR 

	/**
	 * Identify file type of file with provided path and name using JDK 7's
	 * Files.probeContentType(Path).
	 *
	 * @param fileName
	 * Name of file whose type is desired.
	 * @return String representing identified type of file with provided name.
	 */
	public String identifyFileTypeUsingFilesProbeContentType(final String fileName) {
		String fileType = "Undetermined";
		final File file = new File(fileName);
		try {
			fileType = Files.probeContentType(file.toPath());
		} catch (IOException ioException) {
			System.out.println(
					"ERROR: Unable to determine file type for " + fileName + " due to exception " + ioException);
		}
		return (fileType == null) ? "indeterminate" : fileType;
	}

}
