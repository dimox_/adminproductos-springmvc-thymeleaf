package com.dimox.springboot.webapp.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.dimox.springboot.webapp.models.entities.Producto;
import com.dimox.springboot.webapp.models.services.IProductoService;
import com.dimox.springboot.webapp.util.paginator.PageRender;

@Controller
public class ProductosController {

	@Autowired
	private IProductoService productoService;

	@Value("${texto.indexcontroller.tituloProductos}")
	private String tituloProductos;

	@Value("${texto.indexcontroller.tituloGuardarProducto}")
	private String tituloGuardarProducto;

	@GetMapping("/productos")
	public String productos(@RequestParam(name="page", defaultValue="0") Integer page, Model model) {
		
		Pageable pageRequest = PageRequest.of(page, 10);
		
		Page<Producto> productos = productoService.findPaginated(pageRequest);
		
		PageRender<Producto> pageRender = new PageRender<>("/productos", productos);
		
		model.addAttribute("titulo", tituloProductos);
		model.addAttribute("classActiveLinkProductos","nav-link active");
		model.addAttribute("productos", productos);
		model.addAttribute("page", pageRender);
		
		return "listado-productos";
	}

	@GetMapping("/productos/formGuardar")
	public String formularioGuardar(Model model) {
		model.addAttribute("botonSubmit", "Guardar");
		model.addAttribute("titulo", tituloGuardarProducto);
		model.addAttribute("tituloFormulario", "Crear nuevo producto");
		model.addAttribute("producto", new Producto());
		return "form-guardar-productos";
	}

	@GetMapping("/productos/formEditar/{id}")
	public String formularioEditar(@PathVariable Long id, Model model, RedirectAttributes flash) {
		Producto producto = null;
		if (id > 0) {
			producto = productoService.findOne(id);
			if (producto == null) {
				flash.addFlashAttribute("error", "El producto no existe. ");
				return "redirect:/productos";
			}
		} else {
			flash.addFlashAttribute("error", "El ID del producto no es válido. ");
			return "redirect:/productos";
		}
		model.addAttribute("botonSubmit", "Actualizar");
		model.addAttribute("titulo", tituloGuardarProducto);
		model.addAttribute("tituloFormulario", "Editar producto");
		model.addAttribute("producto", producto);
		return "form-guardar-productos";
	}

	@PostMapping("/productos/save")
	public String guardarActualizar(@Valid Producto producto, BindingResult bindingResult, Model model,
			RedirectAttributes flash) {

		if (bindingResult.hasErrors()) {
			model.addAttribute("botonSubmit", "Guardar");
			model.addAttribute("titulo", tituloGuardarProducto);
			return "form-guardar-productos";
		}

		String mensageFlash = (producto.getId_prod() != null) ? "El producto ha sido modificado con éxito!"
				: "El producto ha sido creado con éxito! ";
		try {
			productoService.save(producto);
			flash.addFlashAttribute("success", mensageFlash);
		} catch (Exception e) {
			flash.addFlashAttribute("error", "El producto ya existe, introduzca otro nombre");
			return "redirect:/productos";
		}

		return "redirect:/productos";
	}

	@GetMapping("/productos/delete/{id}")
	public String delete(@PathVariable Long id, RedirectAttributes flash) {
		
		try {
			productoService.delete(id);
			flash.addFlashAttribute("success", "El producto ha sido eliminado con éxito! ");
		} catch (Exception e) {
			flash.addFlashAttribute("error", "No se ha podido eliminar el producto seleccionado ");
		}
		return "redirect:/productos";
	}
	
	@GetMapping("/productos/detalle/{id}")
	public String detalleProducto(@PathVariable Long id, Model model, RedirectAttributes flash) {
		
		Producto producto = productoService.findOne(id);
		if (producto == null) {
			flash.addFlashAttribute("error", "El producto no existe ");
			return "redirect:/productos";
		}
		
		model.addAttribute("producto", producto);
		model.addAttribute("titulo", "Detalle del producto");
		return "detalle-producto";
		
	}
}
