package com.dimox.springboot.webapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootAdminProductosThymeleafApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootAdminProductosThymeleafApplication.class, args);
	}

}
