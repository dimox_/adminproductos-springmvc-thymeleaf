package com.dimox.springboot.webapp.models.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.dimox.springboot.webapp.models.entities.Producto;


public interface IProductoDAO extends PagingAndSortingRepository<Producto, Long> {
	
	@Query("FROM Producto p LEFT JOIN p.categoria_prod c")
	public List<Producto> fetchProductos();  

}

