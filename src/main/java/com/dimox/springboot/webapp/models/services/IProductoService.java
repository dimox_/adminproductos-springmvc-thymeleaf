package com.dimox.springboot.webapp.models.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.dimox.springboot.webapp.models.entities.Producto;

public interface IProductoService {

	public List<Producto> findAll();
	public Page<Producto> findPaginated(Pageable pageable);
	public Producto findOne(Long id);
	public Producto save(Producto producto);
	public void delete(Long id);
}
