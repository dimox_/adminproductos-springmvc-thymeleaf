package com.dimox.springboot.webapp.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.dimox.springboot.webapp.models.DAO.IProductoDAO;
import com.dimox.springboot.webapp.models.entities.Producto;

@Service
public class ProductoServiceImpl implements IProductoService {

	@Autowired
	private IProductoDAO productoDAO;
	
	@Override
	public List<Producto> findAll() {
		return (List<Producto>) productoDAO.fetchProductos();
	}

	@Override
	public Producto findOne(Long id) {
		return productoDAO.findById(id).orElse(null);
	}

	@Override
	public Producto save(Producto producto) {
		return productoDAO.save(producto);
	}

	@Override
	public void delete(Long id) {
		productoDAO.deleteById(id);
	}

	@Override
	public Page<Producto> findPaginated(Pageable pageable) {
		
		return productoDAO.findAll(pageable);
	}
	
	

}
