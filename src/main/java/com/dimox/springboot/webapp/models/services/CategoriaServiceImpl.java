package com.dimox.springboot.webapp.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.dimox.springboot.webapp.models.DAO.ICategoriaDAO;
import com.dimox.springboot.webapp.models.entities.Categoria;

@Service
public class CategoriaServiceImpl implements ICategoriaService {

	@Autowired
	private ICategoriaDAO categoriaDao;
	
	@Override
	public List<Categoria> findAll() {
		return (List<Categoria>) categoriaDao.findAll();
	}

	@Override
	public Categoria findOne(Long id) {
		return categoriaDao.findById(id).orElse(null);
	}

	@Override
	public Categoria save(Categoria categoria) {
		return categoriaDao.save(categoria);
	}

	@Override
	public void delete(Long id) {
		categoriaDao.deleteById(id);
	}

	@Override
	public Page<Categoria> findPaginated(Pageable pageable) {
		return categoriaDao.findAll(pageable);
	}

}
