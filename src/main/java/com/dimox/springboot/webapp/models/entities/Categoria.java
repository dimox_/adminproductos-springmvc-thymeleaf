package com.dimox.springboot.webapp.models.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;


@Entity
@Table(name = "categorias")
public class Categoria implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_cat;

	@Size(min=3)
	@NotEmpty
	@Column(nullable = false, unique = true, length = 80)
	private String nombre_cat;

	@Column
	private String imagen_cat;

	@OneToMany(mappedBy = "categoria_prod", targetEntity = Producto.class, orphanRemoval=true)
	private List<Producto> ProductoList;

	// BUILDERS
	public Categoria() {
		this.ProductoList = new ArrayList<>();
	}

	// GETTERS AND SETTERS
	public List<Producto> getProductoList() {
		return ProductoList;
	}

	public void setProductoList(List<Producto> productoList) {
		ProductoList = productoList;
	}

	public Long getId_cat() {
		return id_cat;
	}

	public void setId_cat(Long id_cat) {
		this.id_cat = id_cat;
	}

	public String getNombre_cat() {
		return nombre_cat;
	}

	public void setNombre_cat(String nombre_cat) {
		this.nombre_cat = nombre_cat;
	}

	public String getImagen_cat() {
		return imagen_cat;
	}

	public void setImagen_cat(String imagen_cat) {
		this.imagen_cat = imagen_cat;
	}

	public void addProducto(Producto prod) {
		ProductoList.add(prod);
	}
	
	public void remuveProducto(Producto prod) {
		ProductoList.remove(prod);
	}

	// HASHCODE AND EQUALS METHODS
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id_cat == null) ? 0 : id_cat.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Categoria other = (Categoria) obj;
		if (id_cat == null) {
			if (other.id_cat != null)
				return false;
		} else if (!id_cat.equals(other.id_cat))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Categoria [id_cat=" + id_cat + ", nombre_cat=" + nombre_cat + ", imagen_cat=" + imagen_cat
				+ ", ProductoList=" + getProductoList() + "]";
	}

}

