package com.dimox.springboot.webapp.models.DAO;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.dimox.springboot.webapp.models.entities.Categoria;

public interface ICategoriaDAO extends PagingAndSortingRepository<Categoria, Long> {

}
