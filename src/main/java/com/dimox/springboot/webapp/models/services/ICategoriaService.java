package com.dimox.springboot.webapp.models.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.dimox.springboot.webapp.models.entities.Categoria;

public interface ICategoriaService {

	public List<Categoria> findAll();
	public Page<Categoria> findPaginated(Pageable pageable);
	public Categoria findOne(Long id);
	public Categoria save(Categoria categoria);
	public void delete(Long id);
	
}
