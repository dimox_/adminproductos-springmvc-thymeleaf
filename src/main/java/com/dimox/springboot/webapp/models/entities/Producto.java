package com.dimox.springboot.webapp.models.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;


@Entity
@Table(name = "productos")
public class Producto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_prod;

	@NotEmpty
	@Size(min=3)
	@Column(nullable = false, unique = true)
	private String nombre_prod;

	@Column(length = 100)
	private String fabricante_prod;

	@Column
	private String detalle_prod;

	@Lob
	private String descripcion_prod;
	
	
	@NotNull
	@Digits(fraction = 2, integer = 20)
	@Column(precision = 10, scale = 2, nullable = false)
	private BigDecimal precio_prod;

	@Digits(fraction = 0, integer = 20)
	@Column
	private int stock_prod;

	@CreationTimestamp
	private LocalDateTime created_at;

	@UpdateTimestamp
	private LocalDateTime updated_at;

	@Valid
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Categoria.class, cascade = { CascadeType.PERSIST,
			CascadeType.MERGE })
	@JoinColumn(name = "categoria", nullable = true, foreignKey = @ForeignKey(name = "categoria_id_fk"))
	private Categoria categoria_prod;

	// BUILDERS
	public Producto() {
	}


	// GETTERS AND SETTERS
	public LocalDateTime getCreate_at() {
		return created_at;
	}

	public LocalDateTime getUpdate_at() {
		return updated_at;
	}

	public Long getId_prod() {
		return id_prod;
	}

	public void setId_prod(Long id_prod) {
		this.id_prod = id_prod;
	}

	public String getNombre_prod() {
		return nombre_prod;
	}

	public void setNombre_prod(String nombre_prod) {
		this.nombre_prod = nombre_prod;
	}

	public String getFabricante_prod() {
		return fabricante_prod;
	}

	public void setFabricante_prod(String fabricante_prod) {
		this.fabricante_prod = fabricante_prod;
	}

	public String getDetalle_prod() {
		return detalle_prod;
	}

	public void setDetalle_prod(String detalle_prod) {
		this.detalle_prod = detalle_prod;
	}

	public String getDescripcion_prod() {
		return descripcion_prod;
	}

	public void setDescripcion_prod(String descripcion_prod) {
		this.descripcion_prod = descripcion_prod;
	}

	public BigDecimal getPrecio_prod() {
		return precio_prod;
	}

	public void setPrecio_prod(BigDecimal precio_prod) {
		this.precio_prod = precio_prod;
	}

	public int getStock_prod() {
		return stock_prod;
	}

	public void setStock_prod(int stock_prod) {
		this.stock_prod = stock_prod;
	}

	public Categoria getCategoria_prod() {
		return categoria_prod;
	}

	public void setCategoria_prod(Categoria categoria_prod) {
		this.categoria_prod = categoria_prod;
	}
	
	// HASHCODE AND EQUALS METHODS
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id_prod == null) ? 0 : id_prod.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Producto other = (Producto) obj;
		if (id_prod == null) {
			if (other.id_prod != null)
				return false;
		} else if (!id_prod.equals(other.id_prod))
			return false;
		return true;
	}

	// TOSTRING METHOD
	@Override
	public String toString() {
		return "Producto [id_prod=" + id_prod + ", nombre_prod=" + nombre_prod + ", fabricante_prod=" + fabricante_prod
				+ ", detalle_prod=" + detalle_prod + ", descripcion_prod=" + descripcion_prod + ", precio_prod="
				+ precio_prod + ", stock_prod=" + stock_prod + ", created_at=" + created_at + ", updated_at="
				+ updated_at + ", categoria_prod=" + categoria_prod.getNombre_cat() + "]";
	}

}

