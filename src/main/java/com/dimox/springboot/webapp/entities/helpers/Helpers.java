package com.dimox.springboot.webapp.entities.helpers;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Helpers {
	public static BigDecimal getBigTwoDecimals(String cantidadStr) {
		return new BigDecimal(cantidadStr).setScale(2, RoundingMode.DOWN);
	}
}
